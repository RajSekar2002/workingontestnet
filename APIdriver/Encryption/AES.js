
const CryptoJS = require('crypto-js');
const { AES } = CryptoJS;
const args = process.argv;
const plaintext = args[2];
const secretKey = args[3]; // 128-bit key
const key = CryptoJS.enc.Utf8.parse(secretKey);

const encrypted = AES.encrypt(plaintext, key).toString();

console.log('Encrypted:', encrypted);
