
/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.18",
  networks:{
    hardhat:{
      forking:{
        enabled:true,
        url:'https://eth-mainnet.g.alchemy.com/v2/RZtgVJ4iXTDD193JavSrn7vVYlfqJQr5'
      },
      chainId: 1,
    }
  }
};

