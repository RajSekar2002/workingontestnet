const fetch = require('node-fetch');

const url = 'https://eth-mainnet.g.alchemy.com/v2/%27RZtgVJ4iXTDD193JavSrn7vVYlfqJQr5%27';
const options = {
  method: 'POST',
  headers: {accept: 'application/json', 'content-type': 'application/json'},
  body: JSON.stringify({
    id: 1,
    jsonrpc: '2.0',
    method: 'eth_getProof',
    params: ['0xe5cB067E90D5Cd1F8052B83562Ae670bA4A211a8']
  })
};

fetch(url, options)
  .then(res => res.json())
  .then(json => console.log(json))
  .catch(err => console.error('error:' + err));